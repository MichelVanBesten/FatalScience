
#![feature(plugin,box_syntax)]

#![plugin(refl_ext)]
extern crate reflection;

mod functions;


fn do_something() {
    println!("called!");
}

#[rfunction(nop)]
fn add(k: &i32, l: &i32) -> i32 {
    println!("{} + {}", k, l);
    k + l
}

#[rfunction(nop)]
fn mult(a: &i32, b: &i32) -> i32 {
    a + b
}

rinit!();


fn main() {
    do_something();
    functions::to_upper(&"Hello".to_string());

    let arg_b_a = Box::new(1);
    let arg_b_b = Box::new(2);
    let args : Vec<Box<std::any::Any>> = vec!(arg_b_a, arg_b_b);
    r_add_caller(args);

    
    
    println!("functions are: {:?}", r_get_functionlist());
}


//use std::any::Any;
// macro_rules! rfunction {
//     ($arg_name:ident 
// }

type MyResult<T> = Result<T, &'static str>;


fn m_add(a: &i32, b: &i32) -> i32 { a + b }

fn wm_add(b_a: Box<std::any::Any>, b_b: Box<std::any::Any>) -> i32 {
    let cast_a = b_a.downcast::<&i32>().unwrap();
    let cast_b = b_b.downcast::<&i32>().unwrap();
    m_add(*cast_a, *cast_b)
}

// fn vwm_add(args: Vec<Box<Any>>) ->MyResult<i32> {
//     cast_vec(args)
//         .and_then(|t| Ok(m_add(..t)))
// }

fn vwm_add(args: Vec<Box<std::any::Any>>) -> MyResult<Box<std::any::Any>> {
    match args.len() {
        2 => {
            //let b1 = args[0].downcast_ref::<i32>();
            //let () = b1;
            Ok(Box::new(m_add(if let Some(val) = args[0].downcast_ref() {val} else { return Err("Wrong type...") },
                     if let Some(val) = args[1].downcast_ref() {val} else { return Err("Wrong type...") })))

                
        },
        _ => Err("function expects 2 args")
    }
}



// fn main() {
//     println!("Calling: {:?}", m_add(1, 2));

//     let arg_b_a = Box::new(1);
//     let arg_b_b = Box::new("hello?");
//     println!("Indirect: {:?}", wm_add(arg_b_a, arg_b_b));
// }
