
extern crate shared_library;
extern crate libc;

struct module_loader {
    
}

impl module_loader {
    let test2 = shared_library::dynamic_library::DynamicLibrary::open(Some(Path::new("misc.dll"))).unwrap();

    let mem_n: *mut () = match unsafe { test2.symbol("get_num") } {
        Ok(s) => s,
        Err(_) => return (),
    };

    let fun: fn() -> i32 = unsafe { std::mem::transmute(mem_n) };
    println!("{:?}", fun());
}

