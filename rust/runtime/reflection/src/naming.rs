
pub fn get_caller_name(fn_name: &String) -> String
{
    "r_".to_string() + fn_name + "_caller"
}

pub fn get_functionlist_name() -> String
{
    "r_get_functionlist".to_string()
}
