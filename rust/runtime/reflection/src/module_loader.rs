

extern crate dynamic_reload;
extern crate boolinator;

use self::dynamic_reload::{DynamicReload, Lib, Symbol, Search, PlatformName, UpdateState};
use self::boolinator::*;
use std::rc::Rc;
use std::time::Duration;
use std::thread::{self, Thread};
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{self, Sender, Receiver};
use std::env;
use std::fs;
use std::path::{Path, PathBuf};
use std::any::Any;
use std::collections::HashMap;
use std::ffi;
use std::io;

use function::*;

use super::*;

//////////////////////////////////////////////////
// public


pub struct RModule {
    functions: Vec<function::Function>,
    lib: Rc<Lib>
        
}
impl RModule {

    
    fn from_lib<'l>(lib: Rc<Lib>) -> ReflResult<RModule> {
        
        let fun = unsafe {
            lib.lib.get::<fn() -> Vec<function::Declaration>>(b"r_get_functionlist")
        };

        match fun {
            Ok(fun) => {
                let func_decls = fun();
                let mut functions = vec!();
    
                for decl in func_decls {
                    let symbol = unsafe {
                        lib.lib.get::< fn(ArgType) -> ReturnType >(&decl.caller_name.clone().into_bytes())
                    };
                    match symbol {
                        Ok(symbol) => functions.push(Function::new( decl.clone(), *symbol )),
                        Err(e)     => println!("Could not load fn: {:?}.", e)
                    }
                }
                
                Ok(RModule { functions: functions, lib: lib.clone() })
            }
            Err(_) => Err(Error::LibraryWithoutReflection(format!("{:?}", lib.lib)))
        }
    }
    
    pub fn get_function(& self, fn_name: String) -> Result<&Function, Error> {
        println!("searching for function: {:?}, got: {:?}", fn_name, &self.functions);
        let found = self.functions.iter()
            .filter(|f| f.declaration.name == fn_name )
            .collect::<Vec<_>>();
        
        match found.len() {
            0 => Err(Error::FunctionNotFound(fn_name)),
            1 => Ok(found[0].clone()),
            _ => Err(Error::FunctionNameAmbigious(fn_name))

        }

    // pub fn call(&self, name: String, args: RArgType) -> RReturnType {

    //     self.call_sender.send( (name,args) );
    //     self.result_receiver.recv().unwrap()
    // }
    }
}


macro_rules! reverse_statements {
    ($a:stmt) => (
        $a;
    );
    
    ($a:stmt, $($a2:stmt),+) => (
        reverse_statements!($($a2),+);
        $a;
    );
}

macro_rules! execute {

    ($main:expr, where: $($a:stmt),+) => (
        {
            reverse_statements!($($a),+);
            $main
        }
    );
}

pub struct ModuleLoader
{
    modules: HashMap<String, RModule>,
    //loader: DynamicReload<'static>
}

impl ModuleLoader
{
    pub fn new() -> ModuleLoader {
        ModuleLoader {modules: HashMap::new()}
    }
    
    pub fn load_modules(& mut self) -> Result<&ModuleLoader, Error> {

        //let path = ModuleLoader::get_module_path().unwrap();
        //let paths = ModuleLoader::get_module_names(path.clone());

        let path =
            try!(env::current_exe()
                 .map_err(|e| Error::IO(e))
                 .and_then(|p| {
                     let path = p.parent().map(Path::to_path_buf);
                     path.ok_or(Error::IO(io::Error::new(io::ErrorKind::Other, "Could not locate executable.")))
                         
                 }));
        
        let names = try!(ModuleLoader::find_libraries(path.clone()));
        
        let mut load_handler = DynamicReload::new(Some(vec!(path.to_str().unwrap())),
                                                  Some("."),
                                                  Search::Default);

        for name in names {

            // let r_name = path.file_stem()
            //     .and_then(|n| n.to_str())
            //     .map(     |n| n.to_string());
            // let name = r_name.unwrap();
            
            if let Ok(lib) = load_handler.add_library(&name, PlatformName::Yes) {
                let mut m = RModule::from_lib(lib);

                if let Ok(m) = m {
                    self.modules.insert(name.clone(), m);
                }
                
                // if m.load_symbols().is_ok() {
                //     self.modules.insert(name.clone(), m);
                // }
            }
            //.and_then(|module| modules.insert(name, *module));
        }

        Ok(self)
    }
    
    fn find_libraries(path: PathBuf) -> Result<Vec<String>, Error> {

        execute! {
            fs::read_dir(path)
                .map(|dir_entries| dir_entries
                     .filter_map(|entry| entry.ok()
                                 .and_then(|entry| maybe_get_name_of_lib(entry.path())))
                     .collect::<Vec<_>>())
                .map_err(|e| Error::IO(e)),

            where:
            let maybe_get_name_of_lib = |path: PathBuf| -> Option<String> {
                match is_file(&path) && has_library_ending(&path) {
                    true => path.file_stem()
                                .and_then(|n| n.to_str())
                                .map(     |n| n.to_string()),
                    false => None } },
            
            let is_file = |p:&PathBuf| -> bool {
                match p.extension().and_then(|p| p.to_str()) {
                    Some("dll") => true,
                    _           => false } },
            
            let has_library_ending = |p:&PathBuf| -> bool {
                match fs::metadata(p) {
                    Ok(md) => md.is_file(),
                    _      => false } }
        }
    }
    
    pub fn get_module(&self, name: &String) -> Result<&RModule, Error> {
        match self.modules.get(name) {
            Some(module) => Ok(module),
            None         => Err(Error::ModuleNotFound(name.clone()))
        }
    }
}

//////////////////////////////////////////////////
// internal handling

// fn worker()
// {
//     let mut modules = Modules::new();
//     let mut reload_handler = DynamicReload::new(Some(vec!["."]),
//                                                 Some("."),
//                                                 Search::Default);

//     match reload_handler.add_library("misc", PlatformName::Yes) {
//         Ok(lib) => modules.add(&lib),
//         Err(e)  => {
//             println!("unable to load lib!");
//             return;
//         }
//     }

//     loop {
//         reload_handler.update(Modules::reload_callback, &mut modules);

//         if modules.modules.len() > 0 {
            
//             let fun: Symbol< fn() -> Vec<RFunctionDecl> > = unsafe {
//                 modules.modules[0].lib.get(b"r_get_functionlist").unwrap()
//             };

//             println!("Got: {:?}", fun());
//         }

//         thread::sleep(Duration::from_millis(2000));
//     }
// }

    
