
use std::any::Any;

use error::*;

#[derive(Debug,PartialEq,Clone)]
pub struct RFunctionParam {
    pub name: String,
    pub ty: RType
}

impl RFunctionParam {
    pub fn new(n: String, t: RType) -> RFunctionParam {
        RFunctionParam {
            name: n,
            ty: t
        }
    }
}

#[derive(Debug,PartialEq,Clone)]
pub struct Declaration {
    pub name: String,
    pub caller_name: String,
    pub inputs: Vec<RFunctionParam>,
    pub output: RType
}

impl Declaration {
    pub fn new(n: String, cn: String, i: Vec<RFunctionParam>, o: RType) -> Declaration {
        Declaration {
            name: n,
            caller_name: cn,
            inputs: i,
            output: o
        }
    }
   

    pub fn to_string(&self) -> String {
        return self.name.clone();
    }
}

pub type RType = String;


pub type ArgType = Vec<Box<Any + Send>>;
pub type ReturnType = Result<Box<Any + Send>, Error>;

#[derive(Debug)]
pub struct Function {
    pub declaration: Declaration,
    symbol: fn(ArgType) -> ReturnType
}
impl Function {
    pub fn new(d: Declaration, f: fn(ArgType)->ReturnType) -> Function {
        Function{ declaration: d, symbol: f }
    }
    
    pub fn call(&self, args: ArgType) -> ReturnType {

        let ref sym = self.symbol;
        
        sym(args)
    }
}
