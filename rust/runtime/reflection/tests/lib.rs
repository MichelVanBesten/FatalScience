
#![feature(plugin,box_syntax)]

#![plugin(reflection_plugin)]
extern crate reflection;

use reflection::*;

use std::any::Any;

/////////////////////////////////////////////////////
// code being reflected:
#[rfunction]
fn add(a: &i32, b: &i32) -> i32 {
    a + b
}

rinit!();
//
/////////////////////////////////////////////////////


#[test]
fn function_registration() {
    let expected = RFunctionDecl::new(
        "add".to_string(),
        "r_add_caller".to_string(),
        vec!(
            RFunctionParam::new("a".to_string(), "&i32".to_string()),
            RFunctionParam::new("b".to_string(), "&i32".to_string())
        ),
        "i32".to_string()
    );
    let found = r_get_functionlist();
    
    assert_eq!(1, found.len());
    assert_eq!(expected, found[0]);
}

#[test]
fn caller_generation() {
    let args : Vec<Box<Any>> = vec!(box 2, box 3);
    let any_result = r_add_caller(args);

    match any_result {
        Ok(res) => {
            let result : &i32 = res.downcast_ref().unwrap();
            assert_eq!(&5, result)
        }
        Err(_) => assert!(false)
    }
}
