

extern crate syntax;
use syntax::codemap::Span;
use syntax::ext::base::*;

pub enum RFunctionAnnotationResult
{
    Success,
    NotAFunction(Span)
}

impl RFunctionAnnotationResult {
    pub fn notify_user(&self, cx: &mut ExtCtxt) {
        match self {
            &RFunctionAnnotationResult::NotAFunction(span) => cx.span_err(span, "Only functions can be annotated with #[rfunction]."),
            &RFunctionAnnotationResult::Success            => ()
        }
    }
}
